package com.itheima.stock.pojo.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author by itheima
 * @Date 2022/1/9
 * @Description 定义封装多内大盘数据的实体类
 */
@ApiModel("大盘数据实体类")
@Data
public class InnerMarketDomain {
    @ApiModelProperty(value = "大盘编码")
    private String code;

    @ApiModelProperty(value = "大盘名称")
    private String name;

    @ApiModelProperty(value = "开盘点")
    private BigDecimal openPoint;

    @ApiModelProperty(value = "当前点")
    private BigDecimal curPoint;

    @ApiModelProperty(value = "前收盘点")
    private BigDecimal preClosePoint;

    @ApiModelProperty(value = "交易量")
    private Long tradeAmt;

    @ApiModelProperty(value = "交易金额")
    private Long tradeVol;

    @ApiModelProperty(value = "涨跌值")
    private BigDecimal upDown;

    @ApiModelProperty(value = "涨幅")
    private BigDecimal rose;

    @ApiModelProperty(value = "振幅")
    private BigDecimal amplitude;

    @ApiModelProperty(value = "当前时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date curTime;
}
